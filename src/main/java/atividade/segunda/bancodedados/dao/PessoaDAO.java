/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atividade.segunda.bancodedados.dao;

import atividade.segunda.bancodedados.domain.Pessoa;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author WilliamFernandoMende
 */
public class PessoaDAO {
    public final String DRIVER = "org.h2.Driver";
    public final String URL = "jdbc:h2:mem:testdb";
    public final String USER = "sa";
    public final String PASSWORD = "sa";
    
    public PessoaDAO(){
        try{
            Class.forName(DRIVER);
        }catch(ClassNotFoundException e){
            e.printStackTrace();
            System.out.println("Driver do banco de dados h2 nao encontrado");
        }
    }
    
    public List<Pessoa> findAll(){
        final String sql = "SELECT * FROM PESSOA";
        List<Pessoa> pessoas = new java.util.ArrayList<>();
        try( Connection con = DriverManager.getConnection(URL, USER, PASSWORD);
              PreparedStatement pst = con.prepareStatement(sql);
                ResultSet rs = pst.executeQuery()){
            while(rs.next()){
                pessoas.add(pessoaMap(rs));
            }
        }catch(SQLException e){
            e.printStackTrace();
            System.out.println("Falha na conexao com o banco de dados H2");
        }
        return pessoas;
    }
    
    
    public Pessoa pessoaMap(ResultSet rs) throws SQLException {
        Pessoa pessoa = new Pessoa();
        pessoa.setId(rs.getInt("Codigo"));
        pessoa.setNome(rs.getString("Nome"));
        pessoa.setDocumento(rs.getString("Documento"));
        pessoa.setTelefone(rs.getString("Telefone"));
        pessoa.setData_nascimento(rs.getDate("Data_Nascimento"));
        return pessoa;
    }
}
