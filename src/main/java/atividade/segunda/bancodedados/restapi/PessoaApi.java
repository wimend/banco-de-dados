/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atividade.segunda.bancodedados.restapi;

import atividade.segunda.bancodedados.dao.PessoaDAO;
import java.util.List;
import org.springframework.web.bind.annotation.GetMapping;
import atividade.segunda.bancodedados.domain.Pessoa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
/**
 *
 * @author WilliamFernandoMende
 */
@RestController
public class PessoaApi {
    
    private PessoaDAO pessoaDAO= new PessoaDAO();
    
    @GetMapping("/pessoas")
    public List<Pessoa> obterPessoas(){
        return pessoaDAO.findAll();
    }
    
}
